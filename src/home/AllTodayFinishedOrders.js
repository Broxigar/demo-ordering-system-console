import React, { Component } from 'react'
import {API} from '../config'
import AllTodayFinishedOrdersTab from '../home/AllTodayFinishedOrdersTab';
// import ReportDishSales from './ReportDishSales';
import { Button, Tabs, Tab,Modal} from 'react-bootstrap';

export default class AllTodayFinishedOrders extends Component {
    constructor(props) {
        super(props)
        this.state = {
            allOrders: [],
            currentTime: null,
            AllTodayUnfinishedOrdersWithOriginDishes: [],
            AllTodayUnfinishedOrdersWithModifiedDishes: [],
            TodayUnfinishedOrdersWithModifiedDishes: [],
            currentDate: null,
            orderCount: null,
            cashSum: null,
            cardSum: null,
            transferSum: null,
            TotalSum: null,
            allTodayOrders: [],
            TodayDistinctID: [],
            CombinedInfo:[],
            showPrintFailed:false,

        }
    }

    componentDidMount(){
     this.getData();
      this.getCurrentDate();
      this.getTodayStat();
    }

    getData() {
      //console.log(API.baseUri+API.getAllDishes)
      fetch(API.baseUri+API.getAllTodayOrders)
          .then((response) => {
              if (response.status === 200) {
                  return response.json()
              } else console.log("Get data error ");
          }).then((json) =>{
          // console.log(json)
          this.setState({
            allTodayOrders: json.TodayOrders,
              TodayDistinctID: json.DistinctID
          })
          console.log(json.DistinctID)
      }).catch((error) => {
          console.log('error on .catch', error);
      }).then(()=>{
          this.CombineTheInfo()
      })
    }

    getDataDESC() {
        //console.log(API.baseUri+API.getAllDishes)
        fetch(API.baseUri+API.getAllTodayOrdersDESC)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
            // console.log(json)
            this.setState({
                allTodayOrders: json.TodayOrders,
                TodayDistinctID: json.DistinctID
            })
            console.log(json.DistinctID)
        }).catch((error) => {
            console.log('error on .catch', error);
        }).then(()=>{
            this.CombineTheInfo()
        })
    }

    FindDishNameNum=(id)=>{
            let AllOrders = this.state.allTodayOrders;
            var AllDishNameNum =[];


            for(let index in AllOrders){
                if(AllOrders[index].orderID ===id){
                    var tempNameNum={};
                    tempNameNum.name = AllOrders[index].name;
                    tempNameNum.DishCount = AllOrders[index].DishCount;
                    tempNameNum.deleted = AllOrders[index].deleted;
                    tempNameNum.price = AllOrders[index].price;
                    tempNameNum.ename = AllOrders[index].ename;
                    // console.log(tempNameNum)
                    AllDishNameNum.push(tempNameNum);

                    var tempOtherInfo ={};
                    tempOtherInfo.orderID = AllOrders[index].orderID;
                    tempOtherInfo.creatTime = AllOrders[index].creatTime;
                }
            }
            return AllDishNameNum
    }

    CombineTheInfo=()=>{
        let AllOrders = this.state.allTodayOrders;
        var OtherInfo = this.state.TodayDistinctID;

        for(let index in OtherInfo){
            for(let i in AllOrders){
                if(OtherInfo[index].id===AllOrders[i].orderID){
                    OtherInfo[index].creatTime = AllOrders[i].creatTime;
                    OtherInfo[index].finalPrice = AllOrders[i].finalPrice;
                    OtherInfo[index].OrderType = AllOrders[i].OrderType;
                    OtherInfo[index].comment = AllOrders[i].comment;
                }
            }
        }
        this.setState({
            CombinedInfo: OtherInfo
        })
    }



    getCurrentDate = () => {
        fetch(API.baseUri+API.getCurrentDate)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
            //console.log(json)

                this.setState({
                    currentDate: json
                })

        })
    }

    getTodayStat = () => {
        fetch(API.baseUri+API.todayStat)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
            if (json.success === true) {
              console.log(json)
                var TotalSum = Math.round((json.cash+json.card+json.transfer) * 100) / 100
                this.setState({
                    orderCount: json.count,
                    cashSum: json.cash,
                    cardSum: json.card,
                    transferSum: json.transfer,
                    TotalSum: TotalSum
                })
            }
            else {
                this.setState({
                    orderCount: 0,
                    cashSum: 0,
                    cardSum: 0,
                    transferSum: 0,
                    TotalSum: 0
                })
            }
        })
    }
    //------------28 April 2019 ---------PrintReceipt Again-------------------------
    handlePrintFailedClose = () => {
        this.setState({
            showPrintFailed: false,

        });
    }
    handlePrintFailedOpen = () => {
        this.setState({showPrintFailed: true});
    }
    //计算点菜总价
    SumUp = (Dishes) => {
        var tempExistArray = []
        var tempExist = Dishes
        // console.log(tempExist)
        for (let index in tempExist) {
            if (tempExist[index].deleted === 0) {
                tempExistArray.push(tempExist[index])
            }
        }
        // console.log(tempExistArray)
        var total = tempExistArray.reduce((sum, price) => {
            return sum + price.DishCount * price.price
        }, 0)
        return Math.round(total * 100) / 100
    }

    PrintReceipt =(array)=>{
        setTimeout(()=>{
            //get table id & 3 prices
            var tableId=0;
            var FINAL = 0;
            var TOTAL =0;
            var DISCOUNT = 0;
            var comment ='';

            fetch(API.baseUri + API.getOrderInfo + "/" + array.id).then((response) => {
                    if (response.status === 200) {
                        return response.json()
                    } else
                        console.log("Get data error ");
                }
            ).then((json) => {
               tableId = json[0].name;
               // TOTAL = json[0].totalPrice;
               FINAL = json[0].finalPrice;
               comment = json[0].comment;
            });
            fetch(API.baseUri + API.getOrderDiscount + "/" + array.id).then((response) => {
                    if (response.status === 200) {
                        return response.json()
                    } else
                        console.log("Get data error ");
                }
            ).then((json) => {
                if(json[0])
                DISCOUNT = json[0].discount;
            });
                setTimeout(()=>{
                    //use 3 prices, table id, dishes fetch the receipt printer
                    var orderInit = this.FindDishNameNum(array.id);

                    TOTAL = this.SumUp(orderInit);
                    //首次 普通菜品
                    var orderInitNormal = []
                    for (let index in orderInit) {
                        if (orderInit[index].type !== "麻辣香锅" && orderInit[index].type !== "特色烤鱼" && orderInit[index].deleted === 0 )
                        {
                            orderInitNormal.push(orderInit[index])
                        }
                    }
                    //首次 麻辣香锅菜品
                    var orderInitSDHP = []
                    for (let index in orderInit) {
                        if (orderInit[index].type === "麻辣香锅"  && orderInit[index].deleted === 0 )
                        {
                            orderInitSDHP.push(orderInit[index])
                        }
                    }
                    //首次 烤鱼菜品
                    var orderInitFish = []
                    for (let index in orderInit) {
                        if (orderInit[index].type === "特色烤鱼"  && orderInit[index].deleted === 0 )
                        {
                            orderInitFish.push(orderInit[index])
                        }
                    }

                    //总 普通菜品
                    var totalNormal = orderInitNormal
                    //总 麻辣香锅菜品
                    var totalSDHP = orderInitSDHP
                    //总 特色烤鱼菜品
                    var totalFish = orderInitFish

                    console.log("前| 再次打印小票 菜品"+ JSON.stringify(totalNormal));
                    console.log("前| 再次打印小票 tableid "+ tableId+ ", OrderID "+array.id+", totalPrice "+TOTAL+", finalPrice "+FINAL+", Discount "+DISCOUNT +", comment "+comment);

                    fetch('http://localhost:3000/printReceipt', {
                        method: "POST",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "tableID": tableId,

                            "orderNormal": totalNormal,
                            "orderSDHP": totalSDHP,
                            "orderFish": totalFish,

                            "DineInTotalPrice": TOTAL,
                            "DineInDiscountedPrice": FINAL,
                            "DineInDiscount": DISCOUNT,
                            "comment": comment,

                        })
                    })
                        .then(res => {
                            //500 print successful
                            if (res.status === 500) {
                                console.log("打印机| 再次打印小票 菜品"+ JSON.stringify(totalNormal));
                                console.log("打印机| 再次打印小票 table id "+ tableId+ ", OrderID "+array.id+", totalPrice "+TOTAL+", finalPrice "+FINAL+", Discount "+DISCOUNT);
                            }
                            //400 printer disconnected
                            else if (res.status === 400 || res.status===404){
                                this.handlePrintFailedOpen();
                                this.setState({
                                    printFailedStatus:'打印机后台 连接失败，请检查'
                                })
                            }
                            //600 print failed
                            else if(res.status ===600){
                                this.handlePrintFailedOpen();
                                this.setState({
                                    printFailedStatus: '小票  打印失败！ 请检查打印机与打印进程',
                                })
                            }
                            // other situations and report error code
                            else{
                                this.handlePrintFailedOpen();
                                this.setState({
                                    printFailedStatus: '小票 打印失败！ 请联系管理员, 错误码 res.status:'+ res.status,
                                })
                            }
                        })
                },300)
        },100)
    };

    //--------------------28 April 2019 ---------Print Receipt Again ---------------end//


    render()
    {
      return (
        <div className="col-lg-12 nova-card cust-border">
          <center><h3><b>{this.state.currentDate}</b></h3></center>
            <center><h4>当日已结账订单数: {this.state.orderCount}, 现金总额：{this.state.cashSum}, 刷卡总额：{this.state.cardSum}, 转账总额：{this.state.transferSum}</h4><h3>总计：{this.state.TotalSum}</h3></center>
            <div className="row">
                <div className="col-lg-5"/>
                <div className="col-lg-1">
                    <Button bsSize="large" bsStyle="success" onClick={()=>{this.getData()}}>正序</Button>
                </div>
                <div className="col-lg-1">
                    <Button bsSize="large" bsStyle="success" onClick={()=>{this.getDataDESC()}}>倒序</Button>
                </div>
                <div className="col-lg-4"/>
            </div>
            <Tabs bsStyle="pills" defaultActiveKey={1} id="uncontrolled-tab-example">
                <Tab eventKey={1}title={"已完成订单总览"} >
                    <AllTodayFinishedOrdersTab
                        DistinctID = {this.state.CombinedInfo}
                        FindName = {this.FindDishNameNum}
                        OrderType={0}
                        PrintReceipt={this.PrintReceipt}
                    />
                </Tab>
                <Tab eventKey={2}  title={"已完成堂吃订单"} >
                    <AllTodayFinishedOrdersTab
                        DistinctID = {this.state.CombinedInfo}
                        FindName = {this.FindDishNameNum}
                        OrderType={1}
                        PrintReceipt={this.PrintReceipt}
                    />
                </Tab>
                <Tab eventKey={3}  title={"已完成外卖订单"} >
                    <AllTodayFinishedOrdersTab
                        DistinctID = {this.state.CombinedInfo}
                        FindName = {this.FindDishNameNum}
                        OrderType={2}
                        PrintReceipt={this.PrintReceipt}
                    />
                </Tab>
                {/*<Tab eventKey={4}  title={"菜品销量报告"} >*/}
                {/*    <ReportDishSales/>*/}
                {/*</Tab>*/}
            </Tabs>
            {/*----------28 March 2019 -----000Print-------------------------//--*/}
            <div>
                <Modal show={this.state.showPrintFailed} onHide={this.handlePrintFailedClose}>

                    <Modal.Body><center> {this.state.printFailedStatus} </center></Modal.Body>
                    <Modal.Footer>
                        <div className="row">
                            <div className="col-lg-4"/>
                            <div className="col-lg-3">
                                <Button className="button5" onClick={this.handlePrintFailedClose}>关闭</Button>
                            </div>
                            <div className="col-lg-1"/>
                            <div className="col-lg-2">
                            </div>
                        </div>
                    </Modal.Footer>
                </Modal>

            </div>

            {/*------------28 March 2019 --------- Before Print-------//*/}
        </div>
      )
    }
}
