import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {Button, Modal, Label, FormGroup, FormControl, SplitButton, MenuItem} from 'react-bootstrap'
import {API} from '../config'
import AuthOptions from '../auth/AuthOptions'
import Personal from '../personal/Personal'

const PrintImg = '/static/img/Print1.png';

export default class CheckDishesDishes extends Component {

  constructor(props) {
    super(props)
    this.authOptions = React.createRef();
    this.state = {
      childValue: null,
      tableDishes: [],
      thispropsmatchparamstableid: null,
      tableModifiedDishes: [],
      tableStatus: "Occupied",
      tableDishes_orderID: null,
      comment: "",
      show: false,
      showPayment: false,
      memberExist: null,
      memberPhoneNum: '',
      memberPoints: null,
      memberDiscount: 0,
      discount: 0,
      sumAfterRedeem: null,
      redeemablePoints: 0,
      valueAccount: '',
      discountAdminVerification: false,
      passwordFieldType: "Password",
      discountOptions: null,
      verifyTableModifiedDishesNumPositiveExist: null,
      verifyTableModifiedDishesNumNegativeExist: null,
      payment: null,
        showModiDelete: false,
        showOrigDelete: false,
        deleteModiDish: null,
        deleteOrigDish: null,

        //--------28 March 2019 ----000Print Before Print
        show000Print:false,
        showPrinting:false,
        printAllStatus:'正在打印...',
        printAddStatus:'正在打印...',
        printFailedStatus:'',
        showPrintAll:false,
        showPrintFailed:false,
        //--------28 March 2019 ----000Print Before Print//
        //---------15 April 2019 -------modal------
        PrintedName:''
        //---------15 April 2019 -------modal------//
    }
  }

  componentDidMount() {
    this.getData();
    this.getModifiedData();
    // console.log(this.props)
  }

  //将新传入的props赋值给现存的props
  componentWillReceiveProps(nextProps) {
    console.log(nextProps)
    if (nextProps.match.params.tableid
        // || nextProps.match.params.orderid
    ) {
      this.props.match.params.tableid = nextProps.match.params.tableid
      // this.props.match.params.orderid = nextProps.match.params.orderid
      this.getData();
      this.getModifiedData();
      // console.log(this.props.match.params.orderid)
    }
  }

  getToken = () => {
      // Retrieves the user token from localStorage
      var user = localStorage.getItem('SHUWEIYUAN');
      var uu = JSON.parse(user);
      console.log(JSON.parse(user));
      if (JSON.parse(user) === null) {
        window.location = '/'
      }
      else {
        return uu.Token
      }
  }

  //获取对应桌号的 点菜信息
  getData = () => {
    console.log(this.props)
    this.setState({tableDishes: []})
    console.log(this.props)
    console.log(this.state.tableDishes)
    fetch(API.baseUri + API.getallTables).then((response) => {
      if (response.status === 200) {
        return response.json()
      } else
        console.log("Get data error ");
      }
    ).then((json) => {
      console.log(json)
      console.log(this.props.match.params.tableid)
      console.log(json[this.props.match.params.tableid - 1].currentOrderID)
      this.setState({
        tableDishes_orderID: json[this.props.match.params.tableid - 1].currentOrderID
      })
    }).catch((error) => {
      console.log('error on .catch', error);
    }).then(() => {
      fetch(API.baseUri + API.getTableDishes + "/" + this.props.match.params.tableid).then((response) => {
        if (response.status === 200) {
          return response.json()
        } else
          console.log("Get data error ");
        }
      ).then((json) => {
        console.log(json)
        console.log(json[0].orderID)
        this.setState({tableDishes: json})
      }).catch((error) => {
        console.log('error on .catch', error);
      }).then(() => {
        console.log(this.state.tableDishes_orderID)
        fetch(API.baseUri + API.getOrderComment + "/" + this.state.tableDishes_orderID).then((response) => {
          if (response.status === 200) {
            return response.json()
          } else
            console.log("Get data error ");
          }
        ).then((json) => {
          console.log(json[0].comment)
          this.setState({comment: json[0].comment})
        }).catch((error) => {
          console.log('error on .catch', error);
        });
      })
    })
  }

  //计算点菜总价
  SumUp = () => {
    var tempExistArray = []
    var tempExist = this.state.tableDishes
    // console.log(tempExist)
    for (let index in tempExist) {
      if (tempExist[index].deleted === 0) {
        tempExistArray.push(tempExist[index])
      }
    }
    // console.log(tempExistArray)
    var total = tempExistArray.reduce((sum, price) => {
      return sum + price.DishCount * price.price
    }, 0)
    return Math.round(total * 100) / 100
  }

  //计算改动菜品总价
  SumUpModified = () => {
    var temTableModifiedDishesNumPositive = []
    var temTableModifiedDishes = this.state.tableModifiedDishes
    for (let index in temTableModifiedDishes) {
      if (temTableModifiedDishes[index].deleted === 0) {
        temTableModifiedDishesNumPositive.push(temTableModifiedDishes[index])
      }
    }
    var total = temTableModifiedDishesNumPositive.reduce((sum, price) => {
      return sum + price.num * price.price
    }, 0)
    return Math.round(total * 100) / 100
  }


  SumUpEntirePrice = () => {
    var total = this.SumUp() + this.SumUpModified();
    return Math.round(total * 100) / 100
  }


  //删除 新增菜品 (将this.state.tableModifiedDishes.deleted = 1)
  deleteModifiedDish = (value) => {
    for (let index in this.state.tableModifiedDishes) {
      if (this.state.tableModifiedDishes[index].id === value.id ) {

        fetch(API.baseUri + API.updateModifiedDeleted + "/" + value.id)
            .then((response) => {
                if (response.status === 200) {
                    this.getModifiedData();
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
                this.handleDeleteModiClose();
            this.deleteModiDishRemove()
        }).catch((error) => {
            console.log('error on .catch', error);
        });
        break;
      }
    }
  }
  //删除已点的菜 waited for review
  deleteDish = (value) => {
    for (let index in this.state.tableDishes) {
      if (this.state.tableDishes[index].id === value.id ){
        fetch(API.baseUri + API.updateOriginDeleted + "/" + value.id)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
                this.handleDeleteOrigClose();
            this.getOriginData();
            this.deleteOrigDishRemove();
        }).catch((error) => {
            console.log('error on .catch', error);
        });
        break;
      }
    }
  }

  //从数据库items表中 获取 菜的信息
  getModifiedData = () => {
    console.log(this.props.match.params.tableid)
    fetch(API.baseUri + API.getModDishes + "/" + this.props.match.params.tableid).then((response) => {
      if (response.status === 200) {
        return response.json()
      } else
        console.log("Get data error ");
      }
    ).then((json) => {
      console.log(json)

      this.setState({tableModifiedDishes: json})
      console.log("tableModified dishes ID ?"+JSON.stringify(json))
    }).catch((error) => {
      console.log('error on .catch', error);
    });
  }
  //从数据库dishMod表中 获取 改动菜的信息
  getOriginData = () => {
    console.log(this.props.match.params.tableid)
    fetch(API.baseUri + API.getTableDishes + "/" + this.props.match.params.tableid).then((response) => {
      if (response.status === 200) {
        return response.json()
      } else
        console.log("Get data error ");
      }
    ).then((json) => {
      console.log(json)
      this.setState({tableDishes: json})
    }).catch((error) => {
      console.log('error on .catch', error);
    })
  }


  //对应按钮 “结账打印” 并刷新table状态为Available
  checkout = () => {
    this.getModifiedData();
    // console.log(this.state.tableDishes_orderID)
    // console.log(this.props.match.params.tableid)
    // console.log(this.sumAfterRedeem())
    // console.log("Phone" + this.state.memberPhoneNum)
   if (this.state.payment !== 1 && this.state.payment !== 2 &&this.state.payment !== 3){
       this.handleShowPayment()
   }
   else if(this.discountedPrice() !==0){
      fetch(API.baseUri + API.checkOut, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        },
        body: JSON.stringify({
          "orderID": this.state.tableDishes_orderID,
          "tableID": this.props.match.params.tableid,
          // "originalPrice": this.SumUpEntirePrice(),
          "finalPrice": this.discountedPrice(),
          "phone": this.state.memberPhoneNum,
          "paymentMethod": this.state.payment
        })
      }).then(res => {
        if (res.status === 200) {
          // console.log(res.json())
          return res.json();
        }
        else if (res.status===401) {
            window.location = '/'
        }
      }).then(json => {
        console.log(json)
        if (json.success === true) {
          this.print()
          this.authOptions.current.getData();
          this.setState({tableDishes: [], tableModifiedDishes: []})
           window.location = '/home'
        }
      })
    }
    else {
      fetch(API.baseUri + API.checkOut, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        },
        body: JSON.stringify({
          "orderID": this.state.tableDishes_orderID,
          "tableID": this.props.match.params.tableid,
          // "originalPrice": this.SumUpEntirePrice(),
          "finalPrice": this.sumAfterRedeem(),
          "phone": this.state.memberPhoneNum,
            "paymentMethod": this.state.payment
        })
      }).then(res => {
        if (res.status === 200) {
          // console.log(res.json())
          return res.json();
        }
        else if (res.status===401) {
            window.location = '/'
        }
      }).then(json => {
        console.log(json)
        if (json.success === true) {
          this.print()
          this.authOptions.current.getData();
          this.setState({tableDishes: [], tableModifiedDishes: [] ,discount:""})
          window.location = '/home'
        }
      })
    }
    console.log("checkOut");
  }

  SDHPExistCalculator = () => {
    //结算首次点单(已删除的+未删除的)麻辣香锅数量
    var tempSDHPorder = []
    var tempO = this.state.tableDishes
    for (let index in tempO) {
      if (tempO[index].type === "麻辣香锅" ) {
        tempSDHPorder.push(tempO[index].DishCount)
      }
    }
    //结算再次添加（已删除的+未删除的）麻辣香锅数量
    var tempM = this.state.tableModifiedDishes
    for (let index in tempM) {
      if (tempM[index].type === "麻辣香锅" ) {
        tempSDHPorder.push(tempM[index].num)
      }
    }
    return tempSDHPorder.length
  }

  SDHPNumberCalculator = () => {
    var tempSDHPorder = [];
    var total;
    var reducer = (accumulator, currentValue) => accumulator + currentValue;

    //结算首次点单麻辣香锅数量
    var tempO = this.state.tableDishes
    for (let index in tempO) {
      if (tempO[index].type === "麻辣香锅" && tempO[index].deleted === 0) {
        tempSDHPorder.push(tempO[index].DishCount)
      }
    }
    //结算再次添加的麻辣香锅数量 （不包含已删除麻辣香锅）
    var temp = this.state.tableModifiedDishes
    for (let index in temp) {
      if (temp[index].type === "麻辣香锅" && temp[index].deleted === 0) {
        tempSDHPorder.push(temp[index].num)
      }
    }

    if (tempSDHPorder.length !== 0) {
      console.log(tempSDHPorder.reduce(reducer));
      total = tempSDHPorder.reduce(reducer)
    } else {
      total = 0;
    }
    return total;
  }

  checkFishExist = () => {
    var tempArray = []
    var temp = this.state.tableDishes
    console.log(this.state.tableDishes)
    console.log(temp)
    for (let index in temp) {
      if (temp[index].subtype === "烤鱼" && temp[index].deleted === 0 ) {
        tempArray.push(temp[index])
      }
    }
    var temp2 = this.state.tableModifiedDishes
    console.log(temp2)
    for (let index in temp2) {
      if (temp2[index].subtype === "烤鱼" && temp2[index].deleted === 0 ) {
        tempArray.push(temp2[index])
      }
    }
    console.log(tempArray)
    return tempArray.length
  }

  checkFishDishesExist = () => {
    var tempArray = []
    var temp = this.state.tableDishes
    var temp2 = this.state.tableModifiedDishes
    console.log(temp)
    for (let index in temp) {
      if (temp[index].type === "特色烤鱼" ) {
        tempArray.push(temp[index])
      }
    }
    for (let index in temp2) {
      if (temp2[index].type === "特色烤鱼" ) {
        tempArray.push(temp2[index])
      }
    }
    console.log(tempArray)
    return tempArray.length
  }

  //监听会员号码输入框input的实时value
  handleChange = (event) => {
    this.setState({
      memberPhoneNum: event.target.value,
      memberExist: null,
      memberPoints: null
    });
  }
  // memberPhoneNum
  //pointsInfo     phone,    /redeem    phone, orderID
  //检查会员积分
  checkMembershipPoint = () => {
    console.log(this.state.memberPhoneNum)
    fetch(API.baseUri + API.pointsInfo, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"phone": this.state.memberPhoneNum})
    }).then(res => {
      if (res.status === 200) {
        return res.json();
      } else
        console.log(res)
    }).then(json => {
      console.log(json)
      if (json.success === true) {
        console.log("会员电话存在")
        // console.log(json.msg.redeemablePoints)
        this.setState({
          memberExist: true,
          memberPoints: json.msg.points,
          memberDiscount: json.msg.discount,
          redeemablePoints: (json.msg.redeemablePoints * 100)
        })
        console.log(this.state.redeemablePoints)
      } else {
        this.setState({memberExist: false})
      }
    })
  }

  redeemMembershipPoint = () => {
    console.log(this.state.memberPhoneNum)
    console.log(this.state.tableDishes_orderID)
    fetch(API.baseUri + API.redeem, {
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({"phone": this.state.memberPhoneNum, "orderID": this.state.tableDishes_orderID})
    }).then(res => {
      if (res.status === 200) {
        return res.json();
      } else
        console.log(res)
    }).then(json => {
      console.log(json)
      if (json.success === true) {
        console.log(json.discount.discount)
        this.setState({discount: json.discount.discount})

        // console.log(json.msg.redeemablePoints)
      } else if (json.success === false) {
        console.log(json.msg)
      }
    })
  }

  activeOrDisabledRedeemButton = () => {
    console.log("activeOrDisabledRedeemButton")
    var disabled = true;
    if (this.state.memberPoints >= 100) {
      disabled = false;
    }
    return disabled
  }

  sumAfterRedeem = () => {
    var sum = this.SumUpEntirePrice()
    if (this.state.discount !== null) {
      sum = this.SumUpEntirePrice() - this.state.discount
      console.log(sum)
    }
    return sum
  }

  memberPointsCalculatorAfterRedeem = () => {
    var sum = this.state.memberPoints - this.state.redeemablePoints
    return sum
  }
  memberPointsCalculatorAfterCheckout = () => {
    var sum = this.state.memberPoints - this.state.redeemablePoints + parseInt((this.sumAfterRedeem()),10)
    return sum
  }

  handleChangeAccount = (e) => {
    this.setState({ valueAccount: e.target.value });
  }
  handleChangePassword = (e) => {
    this.setState({ valuePassword: e.target.value });
  }

  discountOptions = (num) => {
    this.setState({ discountOptions: num });
    console.log(num)
  }

  discountedPrice = () => {
    // console.log((this.state.discountOptions/100));
    var discountedPrice = Math.floor(this.SumUpEntirePrice() * (this.state.discountOptions/100))
    return discountedPrice
  }

  getValidationState = () => {
  const length = this.state.valueAccount.length;
  if (length >= 5) return 'success';
  else if (length > 0 ) return 'warning';

  return null;
  }

  verifyPasswordFieldType = () => {
    if (this.state.passwordFieldType === "Password"){
      this.setState({
        passwordFieldType: "text"
      })
    } else {
      this.setState({
        passwordFieldType: "Password"
      })
    }
  }

  verifyAuthentication = () => {
    const adminAccount = "admin"
    const adminPassWord = "admin"
    if(this.state.valueAccount === adminAccount && this.state.valuePassword === adminPassWord) {
      console.log("Admin登陆成功")
      this.setState({
        discountAdminVerification: true
      })
    }
  }

  handleClose = () => {
    this.setState({
      show: false,
      valueAccount: [],
      discount: 0,
      memberPhoneNum: '',
      valuePassword: '',
      memberExist: null,
      payment: null
    });
  }
    handlePaymentClose = () => {
        this.setState({
            showPayment: false,

        });
    }
    handle000PrintClose = () => {
        this.setState({
            show000Print: false,

        });
    }

    handleDeleteModiClose = () => {
        this.setState({
            showModiDelete: false,

        });
    }
    handleDeleteModiOpen = () => {
        this.setState({showModiDelete: true});
    }
    handle000PrintOpen = () => {
        this.setState({show000Print: true});
    }

   deleteModiDish = (dish) => {
       this.setState({
           deleteModiDish: dish,

       });
   }
    handleDeleteOrigClose = () => {
        this.setState({
            showOrigDelete: false,

        });
    }
    handleDeleteOrigOpen = () => {
        this.setState({showOrigDelete: true});
    }


    //-------28 March 2019------------- BeforeKitchenPrint-----
    handlePrintingClose = () => {
        this.setState({
            showPrinting: false,
        });
    }
    handlePrintingOpen = () => {
        this.setState({showPrinting: true});
    }


    handlePrintAllClose = () => {
        this.setState({
            showPrintAll: false,

        });
    }
    handlePrintAllOpen = () => {
        this.setState({showPrintAll: true});
    }

    handlePrintFailedClose = () => {
        this.setState({
            showPrintFailed: false,

        });
    }
    handlePrintFailedOpen = () => {
        this.setState({showPrintFailed: true});
    }
    //------------------28 March 2019-------------------------------//

    deleteOrigDish = (dish) => {
        this.setState({
            deleteOrigDish: dish,

        });
    }
    deleteOrigDishRemove = () => {
        this.setState({
            deleteOrigDish: null,

        });
    }
    deleteModiDishRemove = () => {
        this.setState({
            deleteModiDish: null,

        });
    }
  handleShow = () => {
    this.setState({show: true});
  }

  handleShowPayment = () => {
        this.setState({showPayment: true});
    }

  //获得【新增】【普通菜品】的【数量】
  verifyModifiedNormalDishesExist = () => {
    var tempArray = []
    var temp = this.state.tableModifiedDishes
    for (let index in temp) {
      if ( temp[index].type !== "麻辣香锅" && temp[index].type !== "特色烤鱼" ) {
        tempArray.push(temp[index])
      }
    }
    console.log(tempArray.length)
    return tempArray.length
  }
  //获得【原始】【普通菜品】的【数量】
  verifyNormalDishesExist = () => {
    var tempArray = []
    var temp = this.state.tableDishes
    for (let index in temp) {
      if ( temp[index].type !== "麻辣香锅" && temp[index].type !== "特色烤鱼" ) {
        tempArray.push(temp[index])
      }
    }
    console.log(tempArray.length)
    return tempArray.length
  }

  //小票打印
  print = () => {
    // console.log(JSON.stringify(this.state.order))
    var orderInit = this.state.tableDishes
    var orderModified = this.state.tableModifiedDishes
    //首次 普通菜品
    var orderInitNormal = []
     for (let index in orderInit) {
      if (orderInit[index].type !== "麻辣香锅" && orderInit[index].type !== "特色烤鱼" && orderInit[index].deleted === 0 )
      {
        orderInitNormal.push(orderInit[index])
      }
    }
    //首次 麻辣香锅菜品
    var orderInitSDHP = []
     for (let index in orderInit) {
      if (orderInit[index].type === "麻辣香锅"  && orderInit[index].deleted === 0 )
      {
        orderInitSDHP.push(orderInit[index])
      }
    }
    //首次 烤鱼菜品
    var orderInitFish = []
     for (let index in orderInit) {
      if (orderInit[index].type === "特色烤鱼"  && orderInit[index].deleted === 0 )
      {
        orderInitFish.push(orderInit[index])
      }
    }
    //后续 普通菜品
    var orderModifiedNormal = []
     for (let index in orderModified) {
      if (orderModified[index].type !== "麻辣香锅" && orderModified[index].type !== "特色烤鱼" && orderModified[index].deleted === 0 )
      {
        orderModifiedNormal.push(orderModified[index])
      }
    }
    //后续 麻辣香锅菜品
    var orderModifiedSDHP = []
     for (let index in orderModified) {
      if (orderModified[index].type === "麻辣香锅"  && orderModified[index].deleted === 0 )
      {
        orderModifiedSDHP.push(orderModified[index])
      }
    }
    //后续 烤鱼菜品
    var orderModifiedFish = [];
     for (let index in orderModified) {
      if (orderModified[index].type === "特色烤鱼"  && orderModified[index].deleted === 0 )
      {
        orderModifiedFish.push(orderModified[index])
      }
    }
    var comment =  this.state.comment
    //总 普通菜品
    var totalNormal = orderInitNormal.concat(orderModifiedNormal)
    //总 麻辣香锅菜品
    var totalSDHP = orderInitSDHP.concat(orderModifiedSDHP)
    //总 特色烤鱼菜品
    var totalFish = orderInitFish.concat(orderModifiedFish)
    var FINAL = 0
    var DISCOUNT = 0
    if (this.discountedPrice() !== 0) {
      // DishPrice = (Math.round(dishPrice * 100) / 100)}
      DISCOUNT = (Math.round((parseFloat(this.SumUpEntirePrice()) - parseFloat(this.discountedPrice())) * 100) / 100);
      //DISCOUNT = parseFloat(this.SumUpEntirePrice()) - parseFloat(this.discountedPrice())
      FINAL = this.discountedPrice()
    } else {
      //DISCOUNT = parseFloat(this.SumUpEntirePrice()) - parseFloat(this.sumAfterRedeem())
      DISCOUNT = (Math.round((parseFloat(this.SumUpEntirePrice()) - parseFloat(this.discountedPrice())) * 100) / 100);
      FINAL = this.sumAfterRedeem()
    }

    fetch('http://localhost:3000/printReceipt', {
        method: "POST",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "tableID": this.props.match.params.tablename,

        "orderNormal": totalNormal,
        "orderSDHP": totalSDHP,
        "orderFish": totalFish,

        "DineInTotalPrice": this.SumUpEntirePrice(),
        "DineInDiscountedPrice": FINAL,
        "DineInDiscount": DISCOUNT,
          "comment": comment,

          })
    } )
        .then(res => {
            //500 print successful
            if (res.status === 500) {
                this.handlePrintingClose();
            }
            //400 printer disconnected
            else if (res.status === 400 || res.status===404){
                this.handlePrintFailedOpen();
                this.handlePrintingClose();
                this.setState({
                    printFailedStatus:'打印机后台 连接失败，请检查'
                })
            }
            //600 print failed
            else if(res.status ===600){
                this.handlePrintFailedOpen();
                this.handlePrintingClose();
                this.setState({
                    printFailedStatus: '小票  打印失败！ 请检查打印机与打印进程',
                })
            }
            // other situations and report error code
            else{
                this.handlePrintFailedOpen();
                this.handlePrintingClose();
                this.setState({
                    printFailedStatus: '小票 打印失败！ 请联系管理员, 错误码 res.status:'+ res.status,
                })
            }
        })
  }

  //PrinterN
  PrintN =(totalNormal,totalF,totalHP,OrigNormalDish, ModNormalDish)=>{

      fetch('http://localhost:3000/KprinterN', {
          method: "POST",
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              // 'Authorization': 'Bearer ' + this.getToken()
          },
          body: JSON.stringify({
              "tableID": this.props.match.params.tablename,
              "orderNormal": totalNormal,
              "orderNormalF": totalF,
              "orderNormalHP": totalHP,

              "comment":  this.state.comment
          })
      })
          .then(res => {
            //500 print successful
              if (res.status === 500) {
                this.handlePrintFailedClose();
                          this.setState({
                              printAllStatus: '炒菜 对单打印成功！'
                          });
                          fetch(API.baseUri + API.PrintOrig, {
                              method: "POST",
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              body: JSON.stringify({
                                  "OrderedOrigDish": OrigNormalDish,
                              })
                          }).then(res => {
                              if (res.status === 200) {
                                  console.log('PrintOrigDishes'+res);
                                  return res.json();
                              }
                              else console.log(res)
                          }).then(()=>{
                              this.getOriginData();
                          });

                          fetch(API.baseUri + API.PrintMod, {
                              method: "POST",
                              headers: {
                                  'Accept': 'application/json',
                                  'Content-Type': 'application/json',
                              },
                              body: JSON.stringify({
                                  "OrderedModDish": ModNormalDish,
                              })
                          }).then(res => {
                              if (res.status === 200) {
                                  console.log('PrintModDishes'+res);
                                  return res.json();
                              }
                              else console.log(res)
                          }).then(()=>{
                              this.getModifiedData();
                          });
                          this.handlePrintingClose();
                      }
              //400 printer disconnected
              else if (res.status === 400 || res.status ===404){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus:'炒菜 打印机后台 连接失败，请检查'
                  })
              }
              //600 print failed
              else if(res.status ===600){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '炒菜 对单 打印失败！ 请检查打印机与打印进程',
                  })
              }
              // other situations and report error code
              else{
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '炒菜 对单 打印失败！ 请联系管理员, 错误码 res.status:'+ res.status,
                  })
              }
          })
      .then(()=>{
          this.getModifiedData();
          this.getOriginData();
      })
  }

  //PrintE
  PrintE = (totalSDHP, OrigEntreeDish, ModEntreeDish)=>{
      fetch('http://localhost:3000/KprinterE' , {
          method: "POST",
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              // 'Authorization': 'Bearer ' + this.getToken()
          },
          body: JSON.stringify({
              "tableID": this.props.match.params.tablename,
              "orderSDHP": totalSDHP, // 小吃
              "comment":  this.state.comment
          })
      })
          .then(res => {
              //500 print successful
              if (res.status === 500) {
                  this.handlePrintFailedClose();
                  this.setState({
                      printAllStatus: '小吃 对单打印成功！'
                  });
                  fetch(API.baseUri + API.PrintOrig, {
                      method: "POST",
                      headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          "OrderedOrigDish": OrigEntreeDish,
                      })
                  }).then(res => {
                      if (res.status === 200) {
                          // console.log('PrintOrigDishes'+res);
                          return res.json();
                      }
                      // else console.log(res)
                  }).then(()=>{
                      this.getOriginData();
                  });

                  fetch(API.baseUri + API.PrintMod, {
                      method: "POST",
                      headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          "OrderedModDish": ModEntreeDish,
                      })
                  }).then(res => {
                      if (res.status === 200) {
                          // console.log('PrintModDishes'+res);
                          return res.json();
                      }
                      // else console.log(res)
                  }).then(()=>{
                      this.getModifiedData();
                  });
                  this.handlePrintingClose();
              }
              //400 printer disconnected
              else if (res.status === 400 || res.status===404){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus:'小吃 打印机后台 连接失败，请检查'
                  })
              }
              //600 print failed
              else if(res.status ===600){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '小吃 对单 打印失败！ 请检查打印机与打印进程',
                  })
              }
              // other situations and report error code
              else{
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '小吃 对单 打印失败！ 请联系管理员, 错误码 res.status:'+ res.status,
                  })
              }
          })
          .then(()=>{
          this.getModifiedData();
          this.getOriginData();
      })
  }

  //Print C
  PrintC = (totalFish, OrigColdDish, ModColdDish)=>{

      fetch('http://localhost:3000/KprinterC', {
          method: "POST",
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              // 'Authorization': 'Bearer ' + this.getToken()
          },
          body: JSON.stringify({
              "tableID": this.props.match.params.tablename,
              "orderFish": totalFish, // 甜点 凉菜菜品
              "comment": this.state.comment
          })
      })
          .then(res => {
              //500 print successful
              if (res.status === 500) {
                  this.handlePrintFailedClose();
                  this.setState({
                      printAllStatus: '凉菜/甜点 对单打印成功！'
                  });
                  fetch(API.baseUri + API.PrintOrig, {
                      method: "POST",
                      headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          "OrderedOrigDish": OrigColdDish,
                      })
                  }).then(res => {
                      if (res.status === 200) {
                          // console.log('PrintOrigDishes'+res);
                          return res.json();
                      }
                      // else console.log(res)
                  }).then(()=>{
                      this.getOriginData();
                  });

                  fetch(API.baseUri + API.PrintMod, {
                      method: "POST",
                      headers: {
                          'Accept': 'application/json',
                          'Content-Type': 'application/json',
                      },
                      body: JSON.stringify({
                          "OrderedModDish": ModColdDish,
                      })
                  }).then(res => {
                      if (res.status === 200) {
                          // console.log('PrintModDishes'+res);
                          return res.json();
                      }
                      // else console.log(res)
                  }).then(()=>{
                      this.getModifiedData();
                  });
                  this.handlePrintingClose();
              }
              //400 printer disconnected
              else if (res.status === 400 || res.status ===404){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus:'凉菜/甜点 打印机后台 连接失败，请检查'
                  })
              }
              //600 print failed
              else if(res.status ===600){
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '凉菜/甜点 对单 打印失败！ 请检查打印机与打印进程',
                  })
              }
              // other situations and report error code
              else{
                  this.handlePrintFailedOpen();
                  this.handlePrintingClose();
                  this.setState({
                      printFailedStatus: '凉菜/甜点 对单 打印失败！ 请联系管理员, 错误码 res.status:'+ res.status,
                  })
              }
          })
          .then(()=>{
          this.getModifiedData();
          this.getOriginData();
      })
  }

  //-----------15 April 2019 --------- Before Print-----
  //厨房 全部打印 判断是否有未打印菜品
    BeforePrintAll=()=>{
        var temp_printed_O = [];
        var temp_printed_M =[];
        var temp_printed;
        var OriginalDishes = this.state.tableDishes;
        var ModifiedDishes = this.state.tableModifiedDishes;
        if(OriginalDishes.length !==0)
        {
            for (let index in OriginalDishes) {
                if (OriginalDishes[index].printed > 0) {
                    var temp_dish_O = OriginalDishes[index].name;
                    temp_printed_O.push(temp_dish_O);
                }
            }
            for (let i in ModifiedDishes){
                if (ModifiedDishes[i].printed > 0) {
                    var temp_dish_M = ModifiedDishes[i].name;
                    temp_printed_M.push(temp_dish_M);
                }
            }

            temp_printed = temp_printed_O.concat(temp_printed_M);

            if(temp_printed.length===0){
                //全部未打印，执行打印
                this.printKitchen(OriginalDishes,ModifiedDishes)
            }
            else{
                //有已打印菜品，请判断
                var stringName='';
                for(let i in temp_printed){
                    stringName=stringName+temp_printed[i]+' ';
                }
                this.setState({
                    PrintedName: stringName, //显示已打印过的菜品名称
                })
                this.handlePrintAllOpen();//出提示框，stringName 已打印
            }
        }
    }

    //厨房 加菜打印 判断 提取 未打印菜品
    BeforePrintAdd=()=>{
        var temp_print;
        var temp_printed_O = [];
        var temp_printed_M =[];
        var OriginalDishes = this.state.tableDishes;
        var ModifiedDishes = this.state.tableModifiedDishes;
        if(OriginalDishes.length !==0)
        {
            for (let index in OriginalDishes) {
                if (OriginalDishes[index].printed === 0) {
                    var temp_dish_O = OriginalDishes[index];
                    temp_printed_O.push(temp_dish_O);
                }
            }
            for (let i in ModifiedDishes){
                if (ModifiedDishes[i].printed === 0) {
                    var temp_dish_M = ModifiedDishes[i];
                    temp_printed_M.push(temp_dish_M);
                }
            }

            temp_print = temp_printed_O.concat(temp_printed_M);
            console.log("BADD"+JSON.stringify(temp_print));
            if(temp_print.length>0){
                //打印所有未打印菜品 temp_print
                this.printKitchen(temp_printed_O,temp_printed_M)
            }
            else{
                //无未打印菜品，提示
                this.handle000PrintOpen()
            }
        }
        else this.handle000PrintOpen();
    }

  //  -----------20 April 2019 --------- Before Print-----//

  //厨房 对单打印
  printKitchen = (Orig, Mod) => {

    this.handlePrintAllClose();
    this.handlePrintingOpen();
    this.setState({
        printAllStatus:'正在打印...',
        printFailedStatus:''
    })

      var orderInit = Orig;
      var orderModified= Mod;

    //首次 麻辣香锅
    var orderInitHP = [];
     for (let index in orderInit) {
      if (orderInit[index].type === "麻辣香锅" && orderInit[index].deleted === 0 )
      {
        orderInitHP.push(orderInit[index])
      }
    }
    //后续 麻辣香锅
    var orderModifiedHP = [];
     for (let index in orderModified) {
      if (orderModified[index].type === "麻辣香锅" && orderModified[index].deleted === 0 )
      {
        orderModifiedHP.push(orderModified[index])
      }
    }
    //总 麻辣香锅
    var totalHP = orderInitHP.concat(orderModifiedHP);

    //-------------------------------------------------------------------------------//
    //首次 特色烤鱼
    var orderInitF = [];
     for (let index in orderInit) {
      if (orderInit[index].type === "特色烤鱼" && orderInit[index].deleted === 0 )
      {
        orderInitF.push(orderInit[index])
      }
    }
    //后续 特色烤鱼
    var orderModifiedF = [];
     for (let index in orderModified) {
      if (orderModified[index].type === "特色烤鱼" && orderModified[index].deleted === 0 )
      {
        orderModifiedF.push(orderModified[index])
      }
    }
    //总 特色烤鱼
    var totalF = orderInitF.concat(orderModifiedF);

    //-------------------------------------------------------------------------------//
    //首次 普通菜品
    var orderInitNormal = [];
     for (let index in orderInit) {
      if (orderInit[index].type !== "特色烤鱼" && orderInit[index].type !== "麻辣香锅" && orderInit[index].type !== "甜点" && orderInit[index].type !== "小吃" && orderInit[index].type !== "凉菜" && orderInit[index].deleted === 0 )
      {
        orderInitNormal.push(orderInit[index])
      }
    }
    //后续 普通菜品
    var orderModifiedNormal = [];
     for (let index in orderModified) {
      if (orderModified[index].type !== "特色烤鱼" && orderModified[index].type !== "麻辣香锅" && orderModified[index].type !== "甜点" && orderModified[index].type !== "小吃" && orderModified[index].type !== "凉菜" && orderModified[index].deleted === 0 )
      {
        orderModifiedNormal.push(orderModified[index])
      }
    }
    //总 普通菜品
    var totalNormal = orderInitNormal.concat(orderModifiedNormal);

    //---------------------20 April 2019 ------------ PrintOrig,PrintMod-------------
     //Print 总init炒菜菜品
      var OrigNormalDish = orderInitNormal.concat(orderInitF,orderInitHP);
      //Print 总Mod 炒菜菜品
      var ModNormalDish = orderModifiedNormal.concat(orderModifiedF,orderModifiedHP);
//---------------------20 April 2019 ------------ PrintOrig,PrintMod-------------End//

    //-------------------------------------------------------------------------------//

    //首次 小吃菜品
    var orderInitSDHP = [];
     for (let index in orderInit) {
      if (orderInit[index].type === "小吃" && orderInit[index].deleted === 0 )
      {
        orderInitSDHP.push(orderInit[index])
      }
    }
    //后续 小吃菜品
    var orderModifiedSDHP = [];
     for (let index in orderModified) {
      if (orderModified[index].type === "小吃" && orderModified[index].deleted === 0 )
      {
        orderModifiedSDHP.push(orderModified[index])
      }
    }
    //总  小吃菜品
    var totalSDHP = orderInitSDHP.concat(orderModifiedSDHP);
      //---------------------20 April 2019 ------------ PrintOrig,PrintMod-------------
      //Print 总init Entree菜品
      var OrigEntreeDish = orderInitSDHP;
      //Print 总Mod 炒菜菜品
      var ModEntreeDish = orderModifiedSDHP;
//---------------------20 April 2019 ------------ PrintOrig,PrintMod-------------End//
    //-------------------------------------------------------------------------------//

    //首次 甜点 凉菜菜品
    var orderInitFish = [];
     for (let index in orderInit) {
      if ((orderInit[index].type === "甜点" || orderInit[index].type === "凉菜" ) && orderInit[index].deleted === 0 )
      {
        orderInitFish.push(orderInit[index])
      }
    }
    //后续 甜点 凉菜菜品
    var orderModifiedFish = [];
     for (let index in orderModified) {
      if ((orderModified[index].type === "甜点" || orderModified[index].type === "凉菜")  && orderModified[index].deleted === 0 )
      {
        orderModifiedFish.push(orderModified[index])
      }
    }
    //总 甜点 凉菜菜品
    var totalFish = orderInitFish.concat(orderModifiedFish);
      //---------------------20 April 2019 ------------ PrintOrig,PrintMod-------------
      //Print 总init Dessert菜品
      var OrigColdDish = orderInitFish;
      //Print 总Mod 炒菜菜品
      var ModColdDish = orderModifiedFish;
//---------------------28 April 2019 ------------ PrintOrig,PrintMod-------------End//

    //-------------------------------------------------------------------------------//

      //Normal Dishes 1, E 1, C 1
      if((totalNormal.length>0 || totalF.length>0 || totalHP.length>0) && totalSDHP.length>0 && totalFish.length>0){
        // setTimeout(()=>{
        //      this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
        //     setTimeout(()=>{
        //         this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
        //       setTimeout(()=>{
        //           this.PrintC(totalFish, OrigColdDish,ModColdDish);
        //           setTimeout(()=>{
        //               this.getModifiedData();
        //               this.getOriginData();
        //           },300)
        //       },300)
        //     },300)
        // })
        this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
        this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
        this.PrintC(totalFish, OrigColdDish,ModColdDish);
        setTimeout(()=>{
          this.getModifiedData();
          this.getOriginData();
        },1000)


    }
    //Normal Dishes 1, E 1, C 0
    else if((totalNormal.length>0  || totalF.length>0 || totalHP.length>0) && totalSDHP.length>0 && totalFish.length===0){

          // setTimeout(()=>{
          //     this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
          //     setTimeout(()=>{
          //         this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
          //         setTimeout(()=>{
          //                 this.getModifiedData();
          //                 this.getOriginData();
          //         },300)
          //     },300)
          // })
        this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
        this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
        setTimeout(()=>{
          this.getModifiedData();
          this.getOriginData();
        },1000)

      }
      //N 1,E 0, C 1
      else if((totalNormal.length>0 || totalF.length>0 || totalHP.length>0) && totalSDHP.length===0 && totalFish.length>0){
      //   setTimeout(()=>{
      //     this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
      //         setTimeout(()=>{
      //             this.PrintC(totalFish, OrigColdDish,ModColdDish);
      //             setTimeout(()=>{
      //                 this.getModifiedData();
      //                 this.getOriginData();
      //             },300)
      //     },300)
      // })
        this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish);
        this.PrintC(totalFish, OrigColdDish,ModColdDish);
        setTimeout(()=>{
          this.getModifiedData();
          this.getOriginData();
        },1000)
      }
      //N 1,E 0, C0
      else if((totalNormal.length>0 || totalF.length>0 || totalHP.length>0) && totalSDHP.length===0 && totalFish.length===0){

          this.PrintN(totalNormal, totalF, totalHP, OrigNormalDish,ModNormalDish)
      }
      //N 0, E 1, C 1
    else if(totalNormal.length===0 && totalF.length===0 && totalHP.length===0 && totalSDHP.length>0 && totalFish.length>0){
          // setTimeout(()=>{
          //         this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
          //         setTimeout(()=>{
          //             this.PrintC(totalFish, OrigColdDish,ModColdDish);
          //             setTimeout(()=>{
          //                 this.getModifiedData();
          //                 this.getOriginData();
          //             },100)
          //         },300)
          // })
        this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish);
        this.PrintC(totalFish, OrigColdDish,ModColdDish);
        setTimeout(()=>{
          this.getModifiedData();
          this.getOriginData();
        },1000)
    }
    // N 0, E 1, C0
     else if (totalNormal.length===0 && totalF.length===0 && totalHP.length===0 && totalSDHP.length>0 && totalFish.length===0){
        this.PrintE(totalSDHP, OrigEntreeDish,ModEntreeDish)
    }

    // N 0, E 0, C1
      else if (totalNormal.length===0 && totalF.length===0 && totalHP.length===0 && totalSDHP.length===0 && totalFish.length>0){
          this.PrintC(totalFish, OrigColdDish,ModColdDish)
      }
      //-----------28 March 2019 --------- 000Print-----
      else {this.handle000PrintOpen();this.handlePrintingClose()}
        //-----------28 March 2019 --------- 000Print-----//
  }


   cash = () =>{
       this.setState({payment: 1})

   }
    card = () =>{
        this.setState({payment: 2})

    }
    transfer = () =>{
        this.setState({payment: 3})

    }

  render() {
    var toHomePage = {
     pathname: '/home',
    }



    var tableModifiedDishes = <div>
        {this.verifyModifiedNormalDishesExist() !== 0 ?
          <div className="cust-border-top">
            {
              this.state.tableModifiedDishes.map((value, key1) => {
                return (<div key={key1}>
                  <div className="dishesUnderLine">
                    {
                      value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 0 ?
                        <div className="row nova-margin">
                            <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg}  alt="Printed"/> :null } </div>
                          <div className="col-lg-6">{value.name}</div>
                          <div className="col-lg-1 ">{value.num}</div>
                          <div className="col-lg-1">{value.price}</div>
                          <div className="col-lg-1">
                            <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                //this.deleteModifiedDish(value)
                                this.deleteModiDish(value);
                                this.handleDeleteModiOpen();
                              }}>删除</Button>
                          </div>
                        </div>
                        :
                        null
                    }
                  </div>
                </div>)
              })
            }
            {
              this.state.tableModifiedDishes.map((value, key1) => {
                return (<div key={key1}>
                  <div className="dishesUnderLine">
                    {
                      value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 1 ?
                        <div className="row nova-margin">
                            <div className="col-lg-1"/>
                          <div className="col-lg-6 strikeThrough">{value.name}</div>
                          <div className="col-lg-1 strikeThrough">{value.num}</div>
                          <div className="col-lg-1 strikeThrough">{value.price}</div>
                        </div>
                        :
                        null
                    }
                  </div>
                </div>)
              })
            }
          </div>
        :null}

    </div>

    return (
      <div>
          {console.log(this.state.comment)}
        {console.log(this.state.tableModifiedDishes)}
      <div className="">
        <div className="col-sm-12 col-lg-2 padding-tables">

          <AuthOptions ref={this.authOptions} parentChildOccupied={this.parentChildOccupied}/>
          <Personal/>

          <div className="site-info nova-padding nova-card cust-border">
            <ul>
              <li>Nova Software
              </li>
              <li>Canberra House</li>
              <li>+61 4 52542687</li>
              <h5>
                <li>info@novasoftware.com.au</li>
              </h5>
            </ul>
          </div>
        </div>
        <div className="">
          <div className="col-lg-10 cust-border nova-card cust-margin-top">
            <center><b><h3>当前桌号: {this.props.match.params.tablename}</h3></b></center>


            {this.verifyNormalDishesExist() !== 0 || this.verifyModifiedNormalDishesExist() !== 0 ?
              <div className="col-lg-4 cust-border nova-card">
                <center><h3><b>普通菜品列表: </b></h3></center>
                {
                  this.state.tableDishes.map((value, key1) => {
                    return (
                      <div className="dishesUnderLine" key={key1}>
                        {
                          value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 0 ?
                          <div className="row nova-margin">
                              <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                            <div className="col-lg-6">{value.name}</div>
                            <div className="col-lg-1">{value.DishCount}</div>
                            <div className="col-lg-1">{value.price}</div>
                            <div className="col-lg-1">
                              <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                //this.deleteDish(value)
                                  this.deleteOrigDish(value);
                                  this.handleDeleteOrigOpen()
                              }}>删除</Button>
                            </div>
                          </div>
                        : null
                        }
                    </div>)
                  })
                }
                {
                  this.state.tableDishes.map((value, key1) => {
                    return (
                      <div className="dishesUnderLine" key={key1}>
                        {
                          value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 1 ?
                          <div className="row nova-margin">
                              <div className="col-lg-1" />
                            <div className="col-lg-6 strikeThrough">{value.name}</div>
                            <div className="col-lg-1 strikeThrough">{value.DishCount}</div>
                            <div className="col-lg-1 strikeThrough">{value.price}</div>
                          </div>
                        : null
                        }
                    </div>)
                  })
                }
                {tableModifiedDishes}
              </div>
            :null}


            {
               this.SDHPExistCalculator() !== 0 ?
               <div className="col-lg-4 cust-border nova-card">
                <center><h3><b>麻辣香锅菜品列表: </b></h3></center>
                <center><h4 className="">麻辣香锅菜品数量: ({this.SDHPNumberCalculator()})</h4></center>
                { this.SDHPNumberCalculator() > 0 && this.SDHPNumberCalculator() < 5 ?
                  <div>
                    <center><h4 className="cust-text1">少于5份麻辣香锅菜品数量!</h4></center>
                  </div>:null}

                  <div className="">
                          {
                            this.state.tableDishes.map((value, key1) => {
                              return (<div key={key1}>
                                <div className="dishesUnderLine">
                                  {
                                    value !== 0 && value.type === "麻辣香锅" && value.deleted === 0
                                      ? <div className="row nova-margin ">
                                          <div >
                                              <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                            <div className="col-lg-6">{value.name}</div>

                                            <div className="col-lg-1">{value.DishCount}</div>
                                            <div className="col-lg-1">{value.price}</div>
                                            <div className="col-lg-1">
                                              <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                                  //this.deleteDish(value)
                                                  this.deleteOrigDish(value);
                                                  this.handleDeleteOrigOpen()
                                                }}>删除</Button>
                                            </div>
                                          </div>
                                        </div>
                                      : null
                                  }
                                </div>
                              </div>)
                            })
                          }
                          {
                            this.state.tableDishes.map((value, key1) => {
                              return (<div key={key1}>
                                <div className="dishesUnderLine">
                                  {
                                    value !== 0 && value.type === "麻辣香锅" && value.deleted === 1
                                      ? <div className="row nova-margin ">
                                          <div >
                                            <div className="col-lg-1" />
                                            <div className="col-lg-6 strikeThrough">{value.name}（原始）</div>
                                            <div className="col-lg-1 strikeThrough">{value.DishCount}</div>
                                            <div className="col-lg-1 strikeThrough">{value.price}</div>
                                          </div>
                                        </div>
                                      : null
                                  }
                                </div>
                              </div>)
                            })
                          }
                          <div className="cust-border-top">
                            {
                              this.state.tableModifiedDishes.map((value, key1) => {
                                return (<div key={key1}>
                                  <div className="dishesUnderLine">
                                    {
                                      value.type === "麻辣香锅" && value.deleted === 0
                                        ? <div className="row nova-margin">
                                              <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                            <div className="col-lg-6">{value.name}</div>

                                            <div className="col-lg-1 ">{value.num}</div>
                                            <div className="col-lg-1">{value.price}</div>
                                            <div className="col-lg-1">
                                              <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                                  //this.deleteModifiedDish(value)
                                                  this.deleteModiDish(value);
                                                  this.handleDeleteModiOpen();
                                                }}>删除
                                              </Button>
                                            </div>
                                          </div>
                                        : null
                                    }

                                  </div>
                                </div>)
                              })
                            }
                            {
                              this.state.tableModifiedDishes.map((value, key1) => {
                                return (<div key={key1}>
                                  <div className="dishesUnderLine">
                                    {
                                       value.type === "麻辣香锅" && value.deleted === 1
                                        ? <div className="row nova-margin">
                                            <div className="col-lg-1" />
                                            <div className="col-lg-6 strikeThrough">{value.name}</div>
                                            <div className="col-lg-1 strikeThrough">{value.num}</div>
                                            <div className="col-lg-1 strikeThrough">{value.price}</div>
                                          </div>
                                        : null
                                    }
                                  </div>
                                </div>)
                              })
                            }
                          </div>
                        </div>

                  </div>
                : null
            }

            {
              this.checkFishDishesExist() !== 0 ?

                 <div className="col-sm-12 col-lg-4 cust-border nova-card">
                   <center><h3><b>特色烤鱼菜品列表:</b></h3></center>
                   {
                     this.checkFishExist() === 0 ?
                     <div>
                       <center><h4 className="cust-text1">未选择烤鱼口味</h4></center>
                     </div>
                     :null
                   }

                    {
                      this.state.tableDishes.map((value, key1) => {
                        return (<div key={key1}>
                            {
                              value !== 0 && value.type === "特色烤鱼" && value.deleted === 0
                                ? <div className="row nova-margin ">
                                      <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                      <div className="col-lg-6">{value.name}</div>
                                      <div className="col-lg-1">{value.DishCount}</div>
                                      <div className="col-lg-1">{value.price}</div>
                                      <div className="col-lg-1">
                                        <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                            //this.deleteDish(value)
                                            this.deleteOrigDish(value);
                                            this.handleDeleteOrigOpen()
                                          }}>删除</Button>
                                      </div>
                                  </div>
                                : null
                            }
                        </div>)
                      })
                    }
                    {
                      this.state.tableDishes.map((value, key1) => {
                        return (<div key={key1}>
                            {
                              value !== 0 && value.type === "特色烤鱼" && value.deleted === 1
                                ? <div className="row nova-margin ">

                                      <div className="col-lg-1" />
                                      <div className="col-lg-6 strikeThrough">{value.name} (烤鱼)</div>
                                      <div className="col-lg-1 strikeThrough">{value.DishCount}</div>
                                      <div className="col-lg-1 strikeThrough">{value.price}</div>

                                  </div>
                                : null
                            }
                        </div>)
                      })
                    }

                    <div className="cust-border-top">
                      {
                        this.state.tableModifiedDishes.map((value, key1) => {
                          return (
                            <div key={key1} className="dishesUnderLine">
                              {
                                value.num > 0 && value.type === "特色烤鱼"
                                  ? <div className="row nova-margin">
                                        <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                      <div className="col-lg-6">{value.name}</div>
                                      <div className="col-lg-1 ">{value.num}</div>
                                      <div className="col-lg-1">{value.price}</div>
                                      <div className="col-lg-1">
                                        <Button className="deleteButton" bsSize="xsmall" bsStyle="danger" onClick={() => {
                                            //this.deleteModifiedDish(value)
                                            this.deleteModiDish(value);
                                            this.handleDeleteModiOpen();
                                          }}>删除
                                        </Button>
                                      </div>
                                    </div>
                                  : null
                              }
                          </div>)
                        })
                      }
                      {
                        this.state.tableModifiedDishes.map((value, key1) => {
                          return (
                            <div className="dishesUnderLine" key={key1}>
                              {
                                value.num < 0 && value.type === "特色烤鱼"
                                  ? <div className="row nova-margin">
                                      <div className="col-lg-1" />
                                      <div className="col-lg-6 strikeThrough">{value.name}</div>

                                      <div className="col-lg-1 strikeThrough">{value.num}</div>
                                      <div className="col-lg-1 strikeThrough">{value.price}</div>
                                    </div>
                                  : null
                              }
                          </div>)
                        })
                      }
                    </div>
                  </div>
                : null
            }
          </div>

          {this.state.comment !== null && this.state.comment !== "" ?
            <div className="nova-card cust-border col-lg-10">
             <center><h4 className="col-lg-9">备注：{this.state.comment}</h4></center>
            </div>:null
          }

          <div className="nova-card cust-border col-lg-10">
            <div className="col-lg-1"/>
            <div className="col-lg-3">下单: AUD${this.SumUp()}</div>
            <div className="col-lg-1"/>
            <div className="col-lg-3">加菜：AUD${this.SumUpModified()}</div>
            <div className="col-lg-1"/>
            <div className="col-lg-3 cust-p-color2">总价：AUD${this.SumUpEntirePrice()}</div>
          </div>

          <div className="nova-card cust-border1 col-lg-10">
            <div className=" nova-margin cust-margin7">
              <Link to={toHomePage}><Button className="col-lg-2 button3" bsStyle="success" onClick={() => {}}>返回</Button></Link>

              <Link to={{
                  pathname: '/home/Dishes/' + this.props.match.params.tableid+'/'+this.props.match.params.tablename,
                  state: {
                    comment: this.state.comment,
                    tableDishes_orderID: this.state.tableDishes_orderID,
                    tableDishes: this.state.tableDishes,
                    tableModifiedDishes: this.state.tableModifiedDishes
                  }
                }}>
                <Button className="col-lg-2 button3 " bsStyle="success" onClick={() => {}}>加菜</Button>
              </Link>

              <Button className="col-lg-2 button3" bsStyle="success" onClick={() => {
                  this.handleShow()
                }}>结账&打印</Button>
                <Button className="col-lg-2 button3" bsStyle="warning" onClick={() => {this.BeforePrintAll()}}>厨房打印（全部）</Button>
                <Button className="col-lg-2 button3" bsStyle="warning" onClick={() => {this.BeforePrintAdd()}}>厨房打印（未打印）</Button>
              {/*-----------28 March 2019 --------- BeforePrint-----*/}
              {/*<Button className="col-lg-2 button3" bsStyle="warning" onClick={() => {this.BeforePrintAll()}}>厨房打印（全部）</Button>*/}
                {/*<Button className="col-lg-2 button3" bsStyle="warning" onClick={() => {this.BeforePrintAdd()}}>厨房打印（加菜）</Button>*/}
               {/*-----------28 March 2019 --------- BeforePrint----//-*/}
            </div>
          </div>
        </div>
      </div>
      <div>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.match.params.tablename}号桌菜品总预览</Modal.Title>
          </Modal.Header>

          {this.verifyNormalDishesExist() !== 0 || this.verifyModifiedNormalDishesExist() !== 0 ?
          <Modal.Body className="modal">
            <center><h4>普通菜品列表：</h4></center>
            <div className="col-sm-12 col-lg-12 ">
                <div className=" nova-card">

                    {
                      this.state.tableDishes.map((value, key1) => {
                        return (
                          <div key={key1}>
                          <div className="dishesUnderLine">
                            {
                              value !== 0 && value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 0
                                ? <div className="row nova-margin ">
                                    <div >
                                        <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                      <div className="col-lg-6">{value.name}</div>

                                      <div className="col-lg-1">{value.DishCount}</div>
                                      <div className="col-lg-1">{value.price}</div>
                                    </div>
                                  </div>
                                : null
                            }
                          </div>
                        </div>)
                      })
                    }

                  {this.verifyModifiedNormalDishesExist() !== 0 ?
                  <div className="">
                    {console.log(this.state.tableModifiedDishes)}
                    {
                      this.state.tableModifiedDishes.map((value, key1) => {
                        return (<div key={key1}>
                          <div className="dishesUnderLine">
                            {
                              value.num > 0 && value.type !== "麻辣香锅" && value.type !== "特色烤鱼" && value.deleted === 0
                                ? <div className="row nova-margin">
                                      <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                    <div className="col-lg-6">{value.name}</div>
                                    <div className="col-lg-1 ">{value.num}</div>
                                    <div className="col-lg-1">{value.price}</div>
                                  </div>
                                : null
                            }
                          </div>
                          </div>)
                        })
                      }
                    </div>
                    :null}

                </div>
            </div>

          </Modal.Body>

          :null}
          <hr/>

          {this.SDHPExistCalculator() !== 0 ?
          <Modal.Body className="modal">
            <center><h4>麻辣香锅菜品列表：</h4></center>
            <div className="col-sm-12 col-lg-12 ">
                <div className=" nova-card">
                    {
                      this.state.tableDishes.map((value, key1) => {
                        return (<div key={key1}>
                          <div className="dishesUnderLine">
                            {
                              value.type === "麻辣香锅" && value.deleted === 0
                                ? <div className="row nova-margin">
                                    <div >
                                        <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                      <div className="col-lg-6">{value.name}</div>
                                      <div className="col-lg-1">{value.DishCount}</div>
                                      <div className="col-lg-1">{value.price}</div>
                                    </div>
                                  </div>
                                : null
                            }
                          </div>
                        </div>)
                      })
                    }

                    {
                      this.state.tableModifiedDishes.map((value, key1) => {
                        return (<div key={key1}>
                          <div className="dishesUnderLine">
                            {
                              value.num > 0 && value.type === "麻辣香锅" && value.deleted === 0
                                ? <div className="row nova-margin">
                                      <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                    <div className="col-lg-6">{value.name}</div>

                                    <div className="col-lg-1 ">{value.num}</div>
                                    <div className="col-lg-1">{value.price}</div>
                                  </div>
                                : null
                            }
                          </div>
                        </div>)
                      })
                    }
                  </div>
              </div>

          </Modal.Body>

          :null}
          <hr/>

          {this.checkFishDishesExist() !== 0 ?
          <Modal.Body className="modal">
              <center><h4>特色烤鱼菜品列表：</h4></center>
              <div className="col-sm-12 col-lg-12 ">
                  <div className=" nova-card">
                      {
                        this.state.tableDishes.map((value, key1) => {
                          return (<div key={key1}>
                            <div className="dishesUnderLine">
                              {
                                value.type === "特色烤鱼" && value.deleted === 0
                                  ? <div className="row nova-margin">
                                      <div >
                                          <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                        <div className="col-lg-6">{value.name}</div>
                                        <div className="col-lg-1">{value.DishCount}</div>
                                        <div className="col-lg-1">{value.price}</div>
                                      </div>
                                    </div>
                                  : null
                              }
                            </div>
                          </div>)
                        })
                      }

                      {
                        this.state.tableModifiedDishes.map((value, key1) => {
                          return (<div key={key1}>
                            <div className="dishesUnderLine">
                              {
                                value.num > 0 && value.type === "特色烤鱼" && value.deleted === 0
                                  ? <div className="row nova-margin">
                                        <div className="col-lg-1 printed">{value.printed!==0?<img src={PrintImg} alt="Printed"/> :null } </div>
                                      <div className="col-lg-6">{value.name}</div>

                                      <div className="col-lg-1 ">{value.num}</div>
                                      <div className="col-lg-1">{value.price}</div>
                                    </div>
                                  : null
                              }
                            </div>
                          </div>)
                        })
                      }
                    </div>
                </div>

            </Modal.Body>
          :null}



          <Modal.Body className="modal">
            <hr/>
            <center>
              <h4 className="modal2">总价：$AUD {this.SumUpEntirePrice()}</h4>
            </center>
          </Modal.Body>

            <Modal.Body className="modal">
                <hr/>

                    <div className="nova-card col-lg-12">
                        <center>
                    <Button className="col-lg-2 button5"  onClick={() => {this.cash()}}>现金</Button>
                    <Button className="col-lg-2 button5"  onClick={() => {this.card()}}>刷卡</Button>
                    <Button className="col-lg-2 button5"  onClick={() => {this.transfer()}}>转账</Button>
                        </center>
                </div>

            </Modal.Body>

          <Modal.Footer>
            <div >
              <div className="col-lg-6 pull-left div-MembershipPoints">
                <form className="" action="/action_page.php">
                  会员号码:
                  <input type="text" placeholder="请输入会员号码" name="LastName" onChange={this.handleChange} value={this.state.memberPhoneNum}/>
                  {
                    this.state.memberExist === true && this.state.discount === 0 && this.state.memberPoints < 100
                      ? <div>
                            <center><Label className="cust-label" bsStyle="info">当前拥有积分：{this.state.memberPoints}分</Label></center><br/>
                            <center><Label className="cust-label" bsStyle="danger">兑换积分不足</Label></center>
                        </div>
                      : null
                  }
                  {
                    this.state.memberExist === true && this.state.discount === 0 && this.state.memberPoints >= 100
                      ? <div>
                            <center><Label className="cust-label" bsStyle="info">当前拥有积分：{this.state.memberPoints}分</Label></center><br/>
                            <center><Label className="cust-label" bsStyle="info">共计可兑换：${this.state.memberDiscount}AUD</Label></center>
                        </div>
                      : null
                  }

                  {
                    this.state.memberExist === true && this.state.discount !== 0
                      ? <div>
                            <center><Label className="cust-label" bsStyle="info">剩余积分： {this.memberPointsCalculatorAfterRedeem()}分</Label></center>
                            <center><Label className="cust-label" bsStyle="info">结账之后积分：{this.memberPointsCalculatorAfterCheckout()}分</Label></center>
                        </div>
                      : null
                  }

                  {
                    this.state.memberExist === false
                      ? <div>
                          <Label bsStyle="danger">此号码没有会员权限</Label>
                        </div>
                      : null
                  }
                </form>

                <center><Button onClick={this.checkMembershipPoint}>查询积分</Button></center>
                {
                  this.state.memberPoints >= 100 && this.state.discount === 0
                    ? <center><Button onClick={this.redeemMembershipPoint}>兑换积分</Button></center>
                    : null
                }
                {
                  this.state.memberPoints >= 100 && this.state.discount !== 0
                    ? <center><Button bsStyle="success">兑换成功</Button></center>
                    : null
                }

                {
                  this.state.discount !== 0
                    ? <div>
                        <Label className="cust-label">{this.SumUpEntirePrice()}- {this.state.discount}</Label>
                        <Label className="cust-label" bsStyle="danger">总价 ：{this.sumAfterRedeem()}</Label>
                      </div>
                    : null
                }
              </div>

              <div className="col-lg-6 pull-left div-MembershipPoints">
                <center><p>调价管理</p></center>
                {console.log(this.discountAdminVerification)}
                {this.state.discountAdminVerification === false ?
                  <div>
                  <form>
                    <FormGroup
                       controlId="formBasicText"
                       validationState={this.getValidationState()}
                     >
                     <FormControl
                       type="text"
                       value={this.state.valueAccount}
                       placeholder="请填写Admin账号"
                       onChange={this.handleChangeAccount}
                     />
                   </FormGroup>
                  </form>

                  <form>
                     <FormGroup
                       controlId="formBasicText1"
                        validationState={this.getValidationState()}
                      >
                      <FormControl
                        type={this.state.passwordFieldType}
                        value={this.state.valuePassword}
                        placeholder="请填写Admin密码"
                        onChange={this.handleChangePassword}
                      />
                      <input type="checkbox" onClick={() => {this.verifyPasswordFieldType()}}/>  Show Password
                      </FormGroup>
                      <center><Button onClick={() => {this.verifyAuthentication()}}>登陆</Button></center>
                  </form>
                </div>
                :
                <div>
                  <p>打折</p>
                   {this.state.discountOptions === null ?
                     <div>
                       <SplitButton
                         title="折扣选择"
                         bsStyle="default"
                         id="dropdown-no-caret">
                         <MenuItem onClick={() => {this.discountOptions(90)}}>打九折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(0)}}>打全折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(80)}}>打八折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(70)}}>打七折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(60)}}>打六折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(50)}}>打五折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(40)}}>打四折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(30)}}>打三折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(20)}}>打两折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(10)}}>打一折</MenuItem>
                       </SplitButton>
                       <p className="cust-font">折前总价：{this.SumUpEntirePrice()}</p>
                     </div>
                     :
                     <div>
                       <SplitButton
                         title={this.state.discountOptions}
                         bsStyle="default"
                         id="dropdown-no-caret">
                         <MenuItem  onClick={() => {this.discountOptions(null)}}>取消打折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(90)}}>打九折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(0)}}>打全折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(80)}}>打八折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(70)}}>打七折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(60)}}>打六折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(50)}}>打五折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(40)}}>打四折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(30)}}>打三折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(20)}}>打两折</MenuItem>
                         <MenuItem onClick={() => {this.discountOptions(10)}}>打一折</MenuItem>
                       </SplitButton>
                       <p className="cust-font">折后总价：{this.SumUpEntirePrice()} * {this.state.discountOptions}% = {this.discountedPrice() + "$"}</p>
                     </div>
                   }
                </div>
              }
              </div>
            </div>
            <div className="col-lg-12 div-checkout-button">
              <Button onClick={this.handleClose}>返回</Button>
              <Button bsStyle="success" onClick={this.checkout}>确认结账</Button>
            </div>
          </Modal.Footer>
        </Modal>
      </div>
      <div>
          <Modal show={this.state.showPayment} onHide={this.handlePaymentClose}>

              <Modal.Body>请选择一个付款方式</Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={this.handlePaymentClose}>
                      Close
                  </Button>

              </Modal.Footer>
          </Modal>
      </div>


          <div>

                      <Modal show={this.state.showModiDelete} onHide={this.handleDeleteModiClose}>

                          <Modal.Body><center>是否确认删除菜品？</center></Modal.Body>
                          <Modal.Footer>
                              <div className="row">
                                  <div className="col-lg-3"/>
                                  <div className="col-lg-2">
                                    <Button className="button5" onClick={this.handleDeleteModiClose}>否</Button>
                                  </div>
                                  <div className="col-lg-1"/>
                                  <div className="col-lg-2">
                                    <Button className="button5" bsStyle="success" onClick={()=>{this.deleteModifiedDish(this.state.deleteModiDish)}}>是</Button>
                                  </div>
                              </div>

                          </Modal.Footer>
                      </Modal>


          </div>
          <div>
                      <Modal show={this.state.showOrigDelete} onHide={this.handleDeleteOrigClose}>

                          <Modal.Body><center>是否确认删除菜品？</center></Modal.Body>
                          <Modal.Footer>
                              <div className="row">
                                  <div className="col-lg-3"/>
                                  <div className="col-lg-2">
                                      <Button className="button5" onClick={this.handleDeleteOrigClose}>否</Button>
                                  </div>
                                  <div className="col-lg-1"/>
                                  <div className="col-lg-2">
                                      <Button className="button5" bsStyle="success" onClick={()=>{this.deleteDish(this.state.deleteOrigDish)}}>是</Button>
                                  </div>

                              </div>

                          </Modal.Footer>
                      </Modal>

          </div>
          {/*------------28 March 2019 ---------Before Print-------*/}
          <div>

              <Modal show={this.state.showPrinting} onHide={()=>this.handlePrintingClose()}>
                  <Modal.Header closeButton />
                  <Modal.Body> {this.state.printAllStatus} </Modal.Body>
              </Modal>

          </div>


          <div>
              <Modal show={this.state.showPrintAll} onHide={()=>this.handlePrintAllClose()}>

                  <Modal.Body><center> 该桌 {this.state.PrintedName} 已被打印过，是否仍全部打印？</center></Modal.Body>
                  <Modal.Footer>
                      <div className="row">
                          <div className="col-lg-2"/>
                          <div className="col-lg-2">
                              <Button className="button5" onClick={()=>this.handlePrintAllClose()}>否</Button>
                          </div>
                          <div className="col-lg-2"/>
                          <div className="col-lg-4">
                              <Button className="button6" bsStyle="success" onClick={()=>{this.printKitchen(this.state.tableDishes,this.state.tableModifiedDishes)}}>是，全部打印</Button>
                          </div>

                      </div>

                  </Modal.Footer>
              </Modal>

          </div>

          {/*------------------28 March 2019 --------- 000Print---------*/}
          <div>
          <Modal show={this.state.show000Print} onHide={this.handle000PrintClose}>

          <Modal.Body>当前没有可打印的菜品</Modal.Body>
          <Modal.Footer>
          <Button variant="secondary" onClick={this.handle000PrintClose}>
          Close
          </Button>

          </Modal.Footer>
          </Modal>
          </div>

          {/*----------28 March 2019 -----000Print-------------------------//--*/}
          <div>
              <Modal show={this.state.showPrintFailed} onHide={this.handlePrintFailedClose}>

                  <Modal.Body><center> {this.state.printFailedStatus} </center></Modal.Body>
                  <Modal.Footer>
                      <div className="row">
                          <div className="col-lg-4"/>
                          <div className="col-lg-3">
                              <Button className="button5" onClick={this.handlePrintFailedClose}>关闭</Button>
                          </div>
                          <div className="col-lg-1"/>
                          <div className="col-lg-2">
                          </div>
                      </div>
                  </Modal.Footer>
              </Modal>

          </div>

          {/*------------28 March 2019 --------- Before Print-------//*/}
    </div>
    )
  }
}
