import React, { Component } from 'react'
import { Button,Table} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { API } from '../config'
import AuthOptions from '../auth/AuthOptions'
import Personal from '../personal/Personal'
import 'react-datepicker/dist/react-datepicker.css';



export default class CheckBookingsDetails extends Component {
    constructor(props) {
        super(props);
        this.authOptions = React.createRef();
        this.state = {
          bookingTableDishes: [],
          bookingTableDetails: []
        }
  }

   componentWillMount() {
      this.getData()
   }

   //随着桌号的变更 进行数据重读
   componentWillReceiveProps(nextProps) {
     // console.log(nextProps)
     // console.log(nextProps.match.params.tableid)
     if (nextProps.match.params.tableid) {
       this.props.match.params.tableid = nextProps.match.params.tableid
       this.getData();
     }
   }

    getData() {
      fetch(API.baseUri + API.getBookingTableDishes + "/" + this.props.match.params.tableid).then((response) => {
        if (response.status === 200) {
          return response.json()
        } else
          console.log("Get data error ");
        }
      ).then((json) => {
        //console.log(json)

        this.setState({bookingTableDishes: json})
        console.log(this.state.bookingTableDishes)
      }).catch((error) => {
        console.log('error on .catch', error);
      }).then(() => {
        fetch(API.baseUri + API.getOrderBookingDetailsbyTableID + "/" + this.props.match.params.tableid).then((response) => {
          if (response.status === 200) {
            return response.json()
          } else
            console.log("Get data error ");
          }
        ).then((json) => {
          //console.log(json)

          this.setState({bookingTableDetails: json})
        }).catch((error) => {
          console.log('error on .catch', error);
        })
      })
    }

    //取消预定 第一步   将预定菜品的  itemsBooking.status = 1
    cancleBookTable = () => {
      console.log(this.props.match.params.tableid)
      fetch(API.baseUri+API.CancleBookingDishes + "/" + this.props.match.params.tableid)
          .then((response) => {
              if (response.status === 200) {
                  console.log("1111")
                  this.cancleBookTableSecond()

              } else console.log("Get data error ");
          }).catch((error) => {
          console.log('error on .catch', error);
      })
    }
    //取消预定第二步
    cancleBookTableSecond = () => {
      fetch(API.baseUri+API.CancleBookTable + "/" + this.props.match.params.tableid)
          .then((response) => {
              if (response.status === 200) {
                  return response.json()

              } else console.log("Get data error ");
          }).then((json) =>{
            this.setState({
              bookingTableDishes:[],
              bookingTableDetails:[]
            })
          //console.log(json)
          window.location = '/home'
      }).catch((error) => {
          console.log('error on .catch', error);
      })
    }

    getToken = () => {
        // Retrieves the user token from localStorage
        var user = localStorage.getItem('SHUWEIYUAN');
        var uu = JSON.parse(user);
        //console.log(JSON.parse(user));
        if (JSON.parse(user) === null) {
          window.location = '/'
        }
        else {
          return uu.Token
        }
    }

    //继承第-步：将预定菜品的  itemsBooking.status = 1
    inherit = () => {
      console.log("继承第1步：")
      fetch(API.baseUri+API.CancleBookingDishes + "/" + this.props.match.params.tableid)
          .then((response) => {
              if (response.status === 200) {
                  this.inheritSecond()
              } else console.log("Get data error ");
          }).catch((error) => {
          console.log('error on .catch', error);
      })
    }
    //继承第二步：改变桌子状态
    inheritSecond = () => {
      console.log("继承第2步：")
      fetch(API.baseUri+API.InheritBookingDishes + "/" + this.props.match.params.tableid)
          .then((response) => {
              if (response.status === 200) {
                  return response.json()
              } else console.log("Get data error ");
          }).then((json) =>{
          //console.log(json)
          this.submitOrder()
      }).catch((error) => {
          console.log('error on .catch', error);
      })
    }

    //继承第三步： 上传菜品信息 后台执行 “/newoder”
    submitOrder = () => {
      console.log("继承第三步：")
      var date = new Date();
      var time = date.toLocaleTimeString();
      var order = this.state.bookingTableDishes
      // var SDHPorder = this.state.SDHPorder
      // var Fishorder = this.state.Fishorder
      var totalorder = order
      //console.log(totalorder)

      fetch(API.baseUri+API.neworder, {
          method: "POST",
          headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.getToken()
        },
        body: JSON.stringify({
                "items": totalorder,
                "creatTime": time,
                "totalPrice": this.state.bookingTableDetails[0].totalPrice,
                "tableID": this.props.match.params.tableid,
                "comment":this.state.bookingTableDetails[0].comment,
            })
      } ).then(res =>{
          if(res.status===200) {
            console.log("status===200")
            this.authOptions.current.getData();
            this.setState({
              bookingTableDishes:[],
              bookingTableDetails:[]
            })
            window.location = '/home/CheckDishes/' + this.props.match.params.tableid
          }
          else console.log(res)
      })
      // .then(json => {
      //    //console.log(json)
      //    console.log(json)
      //   if (json.success === true){
      //     this.authOptions.current.getData();
      //     this.setState({
      //       bookingTableDishes:[],
      //       bookingTableDetails:[]
      //     })
      //     window.location = '/home/CheckDishes/' + this.props.match.params.tableid
      //     // window.location = '/'
      //   }
      // })
    }

    //普通菜品是否存在
    dishExist = () => {
      var tempArray = []
      var temp = this.state.bookingTableDishes
      for (let index in temp) {
        if (temp[index].type !== "麻辣香锅" && temp[index].type !== "特色烤鱼") {
          tempArray.push(temp[index].DishCount)
        }
      }
      return tempArray.length
    }

    //麻辣香锅菜品是否存在
    SDHPdishExist = () => {
      var tempArray = []
      var temp = this.state.bookingTableDishes
      for (let index in temp) {
        if (temp[index].type === "麻辣香锅") {
          tempArray.push(temp[index].DishCount)
        }
      }
      return tempArray.length
    }

    //特色烤鱼菜品是否存在
    FishdishExist = () => {
      var tempArray = []
      var temp = this.state.bookingTableDishes
      for (let index in temp) {
        if (temp[index].type === "特色烤鱼") {
          tempArray.push(temp[index].DishCount)
        }
      }
      return tempArray.length
    }

    //特色烤鱼菜品是否存在
    CommentExist = () => {
      var verifyExist = 0
      //console.log(this.state)
      if (this.state.bookingTableDetails.length !== 0) {
        if (this.state.bookingTableDetails[0].comment !== "") {
          //console.log(this.state.bookingTableDetails[0].comment)
          verifyExist = 1
          return verifyExist
        }

      }
      else {
        return verifyExist
      }
    }
    //
    // getDateTime = () => {
    //   var time = this.state.bookingTableDetails.bookingDateTime
    //   var time2 = time.slice(time.lastIndexOf('T') + 0).replace("Z", "").replace("T", "").replace("+1000", "")
    // }


render() {
  var toHomePage = {
   pathname: '/home',
  }

    return (
      <div>
          <div className="col-sm-12 col-lg-2 padding-tables">

            <AuthOptions
              ref={this.authOptions}
              parentChild={this.parentChild} />
            <Personal />

            <div className="site-info cust-margin3 nova-padding nova-card cust-border">
                <ul>
                    <li>Nova Software </li>
                    <li>Canberra House</li>
                    <li>+61 4 52542687</li>
                    <li>info@novasoftware.com.au</li>
                </ul>
            </div>
          </div>
          <div className="col-lg-10 cust-border nova-card cust-margin-top">
            <center><h3>当前桌号: {this.props.match.params.tablename}</h3></center><br />

          {this.dishExist() !== 0 ?
              <div className="col-lg-4 cust-border nova-card" >
                <center><h3><b>普通菜品列表: </b></h3></center>
                  {
                    this.state.bookingTableDishes.map((value, key1) => {
                      return (
                        <div key={key1} className="dishesUnderLine">
                          {
                             value.type !== "麻辣香锅" && value.type !== "特色烤鱼" ?
                                <div className="row nova-margin">
                                  <div className="col-lg-1" />
                                  <div className="col-lg-6">{value.name}</div>
                                  <div className="col-lg-1">{value.num}</div>
                                  <div className="col-lg-1">{value.price}</div>
                                </div>
                              : null
                          }
                      </div>)
                    })
                  }
              </div>
            :null}

          {this.SDHPdishExist() !== 0 ?
              <div className="col-lg-4 cust-border nova-card" >
                <center><h3><b>麻辣香锅菜品列表: </b></h3></center>
                  {
                    this.state.bookingTableDishes.map((value, key1) => {
                      return (
                        <div key={key1} className="dishesUnderLine">
                          {
                             value.type === "麻辣香锅" ?
                                <div className="row nova-margin">
                                  <div className="col-lg-1" />
                                  <div className="col-lg-6">{value.name}</div>
                                  <div className="col-lg-1">{value.num}</div>
                                  <div className="col-lg-1">{value.price}</div>
                                </div>
                              : null
                          }
                      </div>)
                    })
                  }
              </div>
            :null}

          {this.FishdishExist() !== 0 ?
              <div className="col-lg-4 cust-border nova-card" >
                <center><h3><b>烤鱼菜品列表: </b></h3></center>
                  {
                    this.state.bookingTableDishes.map((value, key1) => {
                      return (
                        <div key={key1} className="dishesUnderLine">
                          {
                             value.type === "特色烤鱼" ?
                                <div className="row nova-margin">
                                  <div className="col-lg-1" />
                                  <div className="col-lg-6">{value.name}</div>
                                  <div className="col-lg-1">{value.num}</div>
                                  <div className="col-lg-1">{value.price}</div>
                                </div>
                              : null
                          }
                      </div>)
                    })
                  }
              </div>
            :null}

                <div className="col-lg-12 cust-border nova-card" >
                    {
                      this.state.bookingTableDetails.map((value, key1) => {


                        return (
                          <div key={key1} className="row nova-margin" >
                              <div className="col-lg-5" />
                              <div className="col-lg-1"><b>菜品总价：</b></div>
                              <div className="col-lg-2"><b>$AUD{value.totalPrice}</b></div>
                                {this.CommentExist() === 1 ?
                                <div>
                                  <div className="col-lg-1" />
                                  <div className="col-lg-1"><b>菜品备注：</b></div>
                                  <div className="col-lg-5"><b>{value.comment}</b></div>
                                </div>
                                :null}

                        </div>)
                      })
                    }
                </div>
          </div>

          <div className="col-lg-10 cust-border nova-card cust-margin-top">
            <div className="col-lg-2 "/ >
            <div className="col-lg-7 cust-border nova-card" >
                  {
                    this.state.bookingTableDetails.map((value, key1) => {
                      return (
                        <div key={key1} >
                          <Table striped bordered condensed hover>
                          <tbody>
                            <tr>
                              <td>顾客信息:</td>
                              <td>姓名：{value.customerName}</td>
                              <td>电话：{value.customerPhoneNO}</td>
                            </tr>
                            <tr>
                              <td>顾客数量:</td>
                              <td colSpan="2">{value.customerNumber}位</td>
                            </tr>
                            <tr>
                              <td colSpan="3"></td>
                            </tr>
                            <tr>
                              <td>预定到店时间:</td>
                              <td>{(value.bookingDateTime.slice(value.bookingDateTime.lastIndexOf('T') + 0)).slice(0, (value.bookingDateTime.slice(value.bookingDateTime.lastIndexOf('T') + 0)).indexOf('.')).replace("T", "")}</td>
                              <td>{value.bookingDateTime.slice(0, value.bookingDateTime.indexOf('T'))}</td>
                            </tr>
                            <tr>
                              <td>预定备注:</td>
                              <td colSpan="2">{value.bookingComment}</td>
                            </tr>
                            <tr>
                              <td>预定生成时间:</td>
                              <td>{value.createTime.slice(value.createTime.lastIndexOf('T') + 0).replace("Z", "").replace("+0000", "").replace("T", "").replace("+1100", "")}</td>
                              <td>{value.createTime.slice(0, value.createTime.indexOf('T'))}</td>
                            </tr>
                          </tbody>
                          </Table>

                          <div>

                            <Button className="col-lg-3" bsSize="large" bsStyle="danger" onClick={()=>{this.cancleBookTable()}}>取消预订</Button>
                            <div className="col-lg-1"></div>
                            <Button className="col-lg-3" bsSize="large" bsStyle="success" onClick={()=>{this.inherit()}}>开始就餐</Button>
                            <div className="col-lg-1"></div>
                            <Link to={toHomePage}><Button className="col-lg-3" bsSize="large" bsStyle="default" onClick={()=>{}}>返回</Button></Link>
                          </div>
                      </div>)
                    })
                  }

            </div>
          </div>
      </div>
    )
}
}
